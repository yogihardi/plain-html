package main

import (
	"fmt"
	"log"
	"net/http"
)

func echoHelloWorld() string {
	return "Hello world!"
}

func page2() string {
	return "Page 2"
}

func page3() string {
	log.Print("page3")
	return "page 3"
}

func main() {
	http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		fmt.Fprint(writer, echoHelloWorld())
	})
	http.HandleFunc("/page2", func(writer http.ResponseWriter, request *http.Request) {
		fmt.Fprint(writer, page2())
	})

	log.Fatal(http.ListenAndServe(":8081", nil))
}
