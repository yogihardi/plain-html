GOARCH?=amd64
PKG_VERSION?=0.0.0
APP_NAME?=helloworld
PACKAGE_NAME?=gitlab.com/yogihardi/plain-html
PKG_LIST := $(shell go list ./... | grep -v /vendor)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

COMMIT=`git rev-parse --short HEAD`

all: lint vet test build

build:
	@GOOS=linux GOARCH=amd64 go build -o $(APP_NAME)-amd64 -a --ldflags "-w \
	-X $(PACKAGE_NAME)/version.Version=$(PKG_VERSION) \
	-X $(PACKAGE_NAME)/version.GitCommit=$(COMMIT)" .

lint: ## Lint the files
	@golint -set_exit_status $(PKG_LIST)

vet: ## Vet the file
	@go vet $(PKG_LIST)

test:
	@go test -short $(PKG_LIST)

clean:
	@rm -rf helloworld*

.PHONY: build lint vet test clean
