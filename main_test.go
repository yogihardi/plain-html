package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestEchoHelloWorld(t *testing.T) {
	assert.Equal(t, echoHelloWorld(), "Hello world!")
}

func TestPage2(t *testing.T) {
	assert.Equal(t, page2(), "Page 2")
}
