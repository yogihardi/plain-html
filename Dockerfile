FROM alpine

ADD helloworld-amd64 helloworld
EXPOSE 8081

ENTRYPOINT ["./helloworld"]