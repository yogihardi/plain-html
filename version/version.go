package version

var (
	// Version package version
	Version = "latest"
	// GitCommit commit hash
	GitCommit = "HEAD"
)
